#include <pcap.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <net/ethernet.h>  // to include struct ethernet
#include <netinet/ip.h>     // Для struct ip
#include <arpa/inet.h>  // to get ntohs
#include "/usr/include/net/if_arp.h"



void process_packet(u_char *arg, const struct pcap_pkthdr *pkthdr,
                    const u_char *packet) {

        struct ether_header *eth_header = (struct ether_header *) packet; // to read first 14 bytes u_char(massive of packet bytes) as ethernet header 
        
        if(ntohs(eth_header->ether_type) == ETHERTYPE_ARP){ // revert bytes from BigEndian to LittleEndian and read type of protocol 
           
           struct arphdr *arp_header = (struct arphdr *)(packet + sizeof(struct ether_header)); // move pointer of packet to the start of information of the arp message
           if(ntohs(arp_header->ar_op) == ARPOP_REQUEST){ //arp request check  : 1-request, 2 - answer

                printf("Ethernet protocol - source: %02x:%02x:%02x:%02x:%02x:%02x", eth_header->ether_shost[0], eth_header->ether_shost[1],
                    eth_header->ether_shost[2], eth_header->ether_shost[3],
                    eth_header->ether_shost[4], eth_header->ether_shost[5]);

                printf(" destination: %02x:%02x:%02x:%02x:%02x:%02x", eth_header->ether_dhost[0], eth_header->ether_dhost[1],
                    eth_header->ether_dhost[2],eth_header->ether_dhost[3],eth_header->ether_dhost[4],eth_header->ether_dhost[5]);



                /*+---------------------------------------------------------------+
                  | Ethernet Header                                                |
                  +---------------------------------------------------------------+
                  | Destination MAC (6 bytes)                                      |
                  +---------------------------------------------------------------+
                  | Source MAC (6 bytes)                                           |
                  +---------------------------------------------------------------+
                  | EtherType (2 bytes, 0x0806 for ARP)                            |
                  +---------------------------------------------------------------+
                  | ARP Header                                                     |
                  +---------------------------------------------------------------+
                  | Hardware type (2 bytes, 1 for Ethernet)                        |
                  +---------------------------------------------------------------+
                  | Protocol type (2 bytes, 0x0800 for IPv4)                       |
                  +---------------------------------------------------------------+
                  | Hardware size (1 byte, 6 for MAC address)                      |
                  +---------------------------------------------------------------+
                  | Protocol size (1 byte, 4 for IP address)                       |
                  +---------------------------------------------------------------+
                  | Opcode (2 bytes, 1 for ARP request, 2 for ARP reply)           |
                  +---------------------------------------------------------------+
                  | Sender MAC address (6 bytes)                                   |
                  +---------------------------------------------------------------+
                  | Sender IP address (4 bytes)                                    |
                  +---------------------------------------------------------------+
                  | Target MAC address (6 bytes)                                   |
                  +---------------------------------------------------------------+
                  | Target IP address (4 bytes)                                    |
                  +---------------------------------------------------------------+
 */    

                unsigned char *sender_ip = (unsigned char *)((unsigned char *)arp_header + sizeof(struct arphdr) + ETH_ALEN);// arp-header = start. sizeof(struct arphdr) -- size of static arp header. ETH_ALEN - size of mac addr
                unsigned char *target_ip = (unsigned char *)((unsigned char *)arp_header + sizeof(struct arphdr) + ETH_ALEN + 4 + ETH_ALEN);
                

                printf(" ARP protocol: who has %d.%d.%d.%3d", target_ip[0], target_ip[1], target_ip[2], target_ip[3]);
                printf(" Tell %d.%d.%d.%3d\n",sender_ip[0], sender_ip[1],sender_ip[2],sender_ip[3]);
            

            



        }
    }


}
int main(int argc, char *argv[]) {
  if (argc != 2) {
    fprintf(stderr, "Expected format: %s <path_to_pcapfile>\n", *argv);
    return EXIT_FAILURE;
  }
  const char *file = argv[1];
  pcap_t *pcap = NULL;
  char error_buffer[PCAP_ERRBUF_SIZE];
  memset(error_buffer, 0, PCAP_ERRBUF_SIZE);

  // Open saved pcap file.
  pcap = pcap_open_offline(file, error_buffer);

  // Check if we were able to open the file.
  if (pcap == NULL) {
    fprintf(stderr, "ERROR: %s\n", error_buffer);
    return EXIT_FAILURE;
  }

  // Loop over all packets and call our handler.
  if (pcap_loop(pcap, -1, process_packet, NULL) == -1) {
    fprintf(stderr, "ERROR: %s\n", pcap_geterr(pcap));
    return EXIT_FAILURE;
  }

  pcap_close(pcap);


  puts("https://www.youtube.com/watch?v=Y_2iRo6j7xc&ab_channel=%D0%90%D0%BB%D0%B5%D0%BA%D1%81%D0%B0%D0%BD%D0%B4%D1%80%D0%93%D1%80%D0%BE%D0%BC%D0%BE%D0%B2");
  return EXIT_SUCCESS;
}